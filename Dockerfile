FROM python:3.7

ADD src/ /
ENV RABBIT_HOST="localhost"
ENV UPLOAD_PATH="/upload/"

RUN mkdir /upload

RUN pip install pika

ENTRYPOINT ["python", "saver.py"]